# Spatial_Data_Manipulation


In this R project, we manipulate spatial data and represent maps about cumulated incidence for Crohn disease between 1990-2014 in France (register EPIMAD). All plots are saved in the result file that you can download in the repository along with the other files (data, r project, script, ...). We use raw SIR, smoothed SIR by bayesian models (iid, bym2).

Authors : Marion Estoup, Michael Genin

E-mail : marion_110@hotmail.fr

June 2023
